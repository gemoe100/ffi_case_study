This case study is used to compare the ruby ffi implementation with the topaz
ffi implementation. It's a gtk application which enables the user to filter
images.

For turning a colored picture into a gray one, run::

  $ ruby grayize.rb path/to/picture.png

This will create ``path/to/gray_picture.png``.

For creating a filtered picture, run::

  $ ruby filter.rb path/to/picture.png (min|max|median|sobel)

This will create ``path/to/filtered_picture.png``.
Other filters can be defined by monkey patching the Array class in
``filter_logic.rb`` (see median and sobel).

The GUI application is started like so::

  $ ruby picmanip.rb

The unit tests are in the folder ``rspec``. The folder ``tests`` contains some
scripts to test whether the filtered pictures look as expected. The scripts
need to be run from the root of the repository.
