require File.expand_path(File.join(File.dirname(__FILE__), "filter_logic.rb"))
require File.expand_path(File.join(File.dirname(__FILE__), "pixbuf.rb"))

module AppFuncs
  def self.pixbuf_filter(pixbuf, &blk)
    reds = []
    greens = []
    blues = []
    pixbuf.each_pixel do |r, g, b|
      reds << r
      greens << g
      blues << b
    end
    res_pixels = []
    [reds, greens, blues].each do |color|
      matrix = color.each_slice(pixbuf.width).to_a
      res = filter(matrix, &blk)
      uchar_scale!(res)
      res_pixels << res.flatten
    end
    pixbuf.each_red_index.zip(res_pixels[0]) do |i, c|
      pixbuf.pixels.put_uchar(i, c)
    end
    pixbuf.each_green_index.zip(res_pixels[1]) do |i, c|
      pixbuf.pixels.put_uchar(i, c)
    end
    pixbuf.each_blue_index.zip(res_pixels[2]) do |i, c|
      pixbuf.pixels.put_uchar(i, c)
    end
  end

  def self.grayize(pixbuf)
    pixbuf.width.times do |i|
      pixbuf.height.times do |j|
        n_channels = pixbuf.n_channels
        rowstride = pixbuf.rowstride
        pixels = pixbuf.pixels
        index = (j*rowstride + i*n_channels)
        rgb = (index..index+n_channels-1).to_a
        gray = rgb.inject(0) { |s, c| s + pixels.get_uchar(c) } / n_channels
        rgb.each { |c| pixels.put_uchar(c, gray) }
      end
    end
  end
end
