require File.expand_path(File.join(File.dirname(__FILE__), "gtk_interface.rb"))
require File.expand_path(File.join(File.dirname(__FILE__), "pixbuf.rb"))
require File.expand_path(File.join(File.dirname(__FILE__), "app_funcs.rb"))

update = proc do |widget|
  width = FFI::MemoryPointer.new :int
  height = FFI::MemoryPointer.new :int
  Gtk::Widget.get_size_request(widget, width, height)
  Gtk::Widget.queue_draw_area(widget, 0, 0, width.read_int, height.read_int)
end

apply_grayize = proc do |image|
  pixbuf = Gtk::Image.get_pixbuf(image)
  AppFuncs.grayize(Pixbuf.new(pixbuf))
  Gtk::Image.set_from_pixbuf(image, pixbuf)
end

apply_filter = proc do |button, image|
  pixbuf = Gtk::Image.get_pixbuf(image)
  label = Gtk::Button.get_label(button)
  AppFuncs.pixbuf_filter(Pixbuf.new(pixbuf)) { |neigh| neigh.send(label) }
  Gtk::Image.set_from_pixbuf(image, pixbuf)
end

def invoke_dialog(button)
  label = Gtk::Button.get_label(button)
  capital_label = label.capitalize
  upcase_label = label.upcase
  dialog = Gtk::FileChooserDialog.new(
    "#{capital_label} Picture",
    Gtk::Widget.get_parent_window(button),
    "GTK_FILE_CHOOSER_ACTION_#{upcase_label}".to_sym,
    :string, "Cancel",
    Gtk::ResponseType, :GTK_RESPONSE_CANCEL,
    :string, capital_label,
    Gtk::ResponseType, :GTK_RESPONSE_ACCEPT,
    :pointer, nil)
  begin
    response = Gtk::Dialog.run(dialog)
    accept = (response == :GTK_RESPONSE_ACCEPT)
    filename = Gtk::FileChooser.get_filename(dialog) if accept
  ensure
    Gtk::Widget.destroy(dialog)
  end
  filename
end

open_pic = proc do |button, image|
  pixbuf = Gtk::Image.get_pixbuf(image)
  filename = invoke_dialog(button)
  if filename
    loaded = Pixbuf.new(filename)
    Gtk::Image.set_from_pixbuf(image, loaded.unwrap)
    G::Object.unref(pixbuf)
  end
end

save_pic = proc do |button, image|
  pixbuf = Gtk::Image.get_pixbuf(image)
  filename = invoke_dialog(button)
  Pixbuf.new(pixbuf).save(filename) if filename
end

# converting argv from ruby to c
argv = FFI::MemoryPointer.new(:pointer, ARGV.length)
ARGV.each_with_index do |arg, i|
  argv[i].write_pointer(FFI::MemoryPointer.from_string(arg))
end

argc_ptr = FFI::MemoryPointer.new(:int)
argc_ptr.write_uint32(ARGV.length)
argv_ptr = FFI::MemoryPointer.new(:pointer)
argv_ptr.write_pointer(argv)
Gtk.init(argc_ptr, argv_ptr)
G::type_init()

window = Gtk::Window.new(:GTK_WINDOW_TOPLEVEL)

# setup buttons for picture manipulation
b_grayize = Gtk::Button.new_with_label("grayize")
b_quit = Gtk::Button.new_with_label("quit")
b_filters = ['min', 'max', 'median', 'sobel'].map do |label|
  Gtk::Button.new_with_label(label)
end

# setup image for displaying the results
pixbuf = Gdk::PixbufMethod.new(:GDK_COLORSPACE_RGB, false, 8, 100, 100)
image = Gtk::Image.new_from_pixbuf(pixbuf)

# setup buttons for dialog
b_open = Gtk::Button.new_with_label("open")
b_save = Gtk::Button.new_with_label("save")

# connect signals
G::Signal.connect(window, "destroy", Gtk::CALLBACK_main_quit, nil)
G::Signal.connect_swapped(b_grayize, "clicked", apply_grayize, image)
G::Signal.connect_swapped(b_grayize, "clicked", update, image)
G::Signal.connect_swapped(b_quit, "clicked", Gtk::CALLBACK_main_quit, window)
b_filters.each do |button|
  G::Signal.connect(button, "clicked", apply_filter, image)
  G::Signal.connect_swapped(button, "clicked", update, image)
end
G::Signal.connect(b_open, "clicked", open_pic, image)
G::Signal.connect_swapped(b_open, "clicked", update, image)
G::Signal.connect(b_save, "clicked", save_pic, image)

# define layout
first_row = Gtk::Box.new(:GTK_ORIENTATION_HORIZONTAL, 10)
second_row = Gtk::Box.new(:GTK_ORIENTATION_HORIZONTAL, 10)
layout = Gtk::Box.new(:GTK_ORIENTATION_VERTICAL, 10)
Gtk::Box.pack_start(first_row, b_open, false, false, 0)
Gtk::Box.pack_start(first_row, b_save, false, false, 0)
Gtk::Box.pack_start(first_row, b_quit, false, false, 0)
Gtk::Box.pack_start(second_row, b_grayize, false, false, 0)
b_filters.each do |button|
  Gtk::Box.pack_start(second_row, button, false, false, 0)
end
Gtk::Box.pack_start(layout, first_row, false, false, 0)
Gtk::Box.pack_start(layout, second_row, false, false, 0)
Gtk::Box.pack_start(layout, image, true, true, 0)
Gtk::Container.add(window, layout)
Gtk::Widget.show_all(window)

Gtk::main()
