raise Exception.new("I need a filename!") if ARGV.empty?
require File.expand_path(File.join(File.dirname(__FILE__), "app_funcs.rb"))

G.type_init()
pixbuf = Pixbuf.new(ARGV[0])
AppFuncs.pixbuf_filter(pixbuf) { |neigh| neigh.send(ARGV[1]) }
full_path_list = ARGV[0].split('/')
path = full_path_list.first(full_path_list.length-1).join('/')
name = full_path_list.last
filename = File.join(path, 'filtered_' << name)
pixbuf.save(filename)
