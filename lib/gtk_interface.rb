require 'ffi'

def init_or_get_module(name, context=Kernel)
  begin
    context.const_get(name)
  rescue
    new_mod = Module.new
    context.const_set(name, new_mod)
    new_mod
  end
end

def interface(libhint, libname, classname, methname, *args)
  lib = init_or_get_module(libname.capitalize)
  camel_case_classname = classname.to_s.split('_').map { |s|
    s.capitalize }.join('')
  cls = init_or_get_module(camel_case_classname, lib)
  cls.extend FFI::Library
  cls.ffi_lib libhint if libhint
  fullname = [libname, classname, methname].join('_')
  cls.attach_function(methname, fullname, *args)
end

module Gtk
  extend FFI::Library
  ffi_lib 'gtk-3'
  attach_function(:init, :gtk_init, [:pointer, :pointer], :void)
  attach_function(:main, :gtk_main, [], :void)
  attach_function(:main_quit, :gtk_main_quit, [], :void)

  CALLBACK_main_quit = proc { main_quit }

  WindowType = enum(:GTK_WINDOW_TOPLEVEL, :GTK_WINDOW_POPUP)
  Orientation = enum(:GTK_ORIENTATION_HORIZONTAL, :GTK_ORIENTATION_VERTICAL)
  FileChooserAction = enum(:GTK_FILE_CHOOSER_ACTION_OPEN,
                           :GTK_FILE_CHOOSER_ACTION_SAVE,
                           :GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER,
                           :GTK_FILE_CHOOSER_ACTION_CREATE_FOLDER)
  ResponseType = enum(:GTK_RESPONSE_NONE, -1,
                      :GTK_RESPONSE_REJECT, -2,
                      :GTK_RESPONSE_ACCEPT, -3,
                      :GTK_RESPONSE_DELETE_EVENT , -4,
                      :GTK_RESPONSE_OK , -5,
                      :GTK_RESPONSE_CANCEL , -6,
                      :GTK_RESPONSE_CLOSE , -7,
                      :GTK_RESPONSE_YES , -8,
                      :GTK_RESPONSE_NO , -9,
                      :GTK_RESPONSE_APPLY , -10,
                      :GTK_RESPONSE_HELP , -11)
end

interface('gtk-3', :gtk, :window, :new, [Gtk::WindowType], :pointer)

interface('gtk-3', :gtk, :widget, :show, [:pointer], :void)
interface(nil, :gtk, :widget, :show_all, [:pointer], :void)
interface(nil, :gtk, :widget, :set_size_request, [:pointer, :int, :int], :void)
interface(nil, :gtk, :widget, :get_size_request,
          [:pointer, :pointer, :pointer], :void)
interface(nil, :gtk, :widget, :get_parent_window, [:pointer], :pointer)
interface(nil, :gtk, :widget, :get_parent_window, [:pointer], :pointer)
interface(nil, :gtk, :widget, :queue_draw_area,
          [:pointer, :int, :int, :int, :int], :void)
interface(nil, :gtk, :widget, :destroy, [:pointer], :void)

interface('gtk-3', :gtk, :label, :new, [:string], :pointer)
interface(nil, :gtk, :label, :set_text, [:pointer, :string], :void)

interface('gtk-3', :gtk, :button, :new_with_label, [:string], :pointer)
interface(nil, :gtk, :button, :get_label, [:pointer], :string)

interface('gtk-3', :gtk, :image, :new_from_pixbuf, [:pointer], :pointer)
interface(nil, :gtk, :image, :get_pixbuf, [:pointer], :pointer)

interface('gtk-3', :gtk, :container, :add, [:pointer, :pointer], :void)

interface('gtk-3', :gtk, :box, :new, [Gtk::Orientation, :int], :pointer)
interface(nil, :gtk, :box, :pack_start,
          [:pointer, :pointer, :bool, :bool, :uint], :void)

interface('gtk-3', :gtk, :dialog, :run, [:pointer], Gtk::ResponseType)

interface('gtk-3', :gtk, :drawing_area, :new, [], :pointer)

interface('gtk-3', :gtk, :file_chooser_dialog, :new,
          [:string, :pointer, Gtk::FileChooserAction, :varargs], :pointer)

interface('gtk-3', :gtk, :file_chooser, :get_filename, [:pointer], :string)

interface('gtk-3', :gtk, :image, :new_from_pixbuf, [:pointer], :pointer)
interface(nil, :gtk, :image, :set_from_pixbuf, [:pointer, :pointer], :void)
interface(nil, :gtk, :image, :get_pixbuf, [:pointer], :pointer)

module Gdk
  module PixbufMethod
    extend FFI::Library
    ffi_lib 'gdk_pixbuf-2.0'

    Colorspace = enum(:GDK_COLORSPACE_RGB)

    attach_function(:new_from_file, :gdk_pixbuf_new_from_file,
                    [:string, :pointer], :pointer)
    #                         ^ to a GError
    attach_function(:save, :gdk_pixbuf_save,
		    [:pointer, :string, :string, :pointer, :varargs], :bool)
    attach_function(:get_width, :gdk_pixbuf_get_width,
                    [:pointer], :int)
    attach_function(:get_height, :gdk_pixbuf_get_height,
                    [:pointer], :int)
    attach_function(:get_n_channels, :gdk_pixbuf_get_n_channels,
                    [:pointer], :int)
    attach_function(:get_rowstride, :gdk_pixbuf_get_rowstride,
                    [:pointer], :int)
    attach_function(:get_pixels, :gdk_pixbuf_get_pixels,
                    [:pointer], :pointer)
    attach_function(:new, :gdk_pixbuf_new,
                    [Colorspace, :bool, :int, :int, :int], :pointer)
    attach_function(:copy_area, :gdk_pixbuf_copy_area,
                    [:pointer, :int, :int, :int, :int, :pointer, :int, :int],
                    :void)
  end
end

module G
  extend FFI::Library
  ffi_lib 'gtk-3'
  attach_function(:type_init, :g_type_init, [], :void)

  ConnectFlags = enum(:G_CONNECT_AFTER, 1, :G_CONNECT_SWAPPED, 2)

  module Signal
    extend FFI::Library
    ffi_lib 'gtk-3'
    callback :callback, [:pointer, :pointer, :pointer], :void
    attach_function(:connect_object, :g_signal_connect_object,
                    [:pointer, :string, :callback, :pointer, ConnectFlags],
                    :ulong)
    class << self
      def connect_after(instance, detailed_signal, c_handler, data)
        connect_object(instance, detailed_signal, c_handler, data,
                       :G_CONNECT_AFTER)
      end
      alias :connect :connect_after
      def connect_swapped(instance, detailed_signal, c_handler, data)
        connect_object(instance, detailed_signal, c_handler, data,
                       :G_CONNECT_SWAPPED)
      end
    end
  end
end
interface("gtk-3", :g, :object, :unref, [:pointer], :void)
