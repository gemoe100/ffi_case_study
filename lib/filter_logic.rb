def alloc_matrix(width, height, elm=0)
  height.times.inject([]) { |matrix| matrix << [elm] * width }
end

def neighbors(matrix, indices)
  x, y = indices
  res = []
  w = matrix.length
  h = matrix[0].length
  [[0, y-1].max, y, [w-1, y+1].min].each do |j|
    [[0, x-1].max, x, [h-1, x+1].min].each do |i|
      res << matrix[j][i]
    end
  end
  res
end

def filter(matrix, &blk)
  h = matrix.length
  w = matrix[0].length
  res = alloc_matrix(w, h)
  h.times do |j|
    w.times do |i|
      res[j][i] = blk.call(neighbors(matrix, [i, j]))
    end
  end
  res
end

def uchar_scale!(matrix)
  flat = matrix.flatten
  min = flat.min
  max = flat.max
  return if min.between?(0, 255) and max.between?(0, 255)

  slope = 255 / (max - min).to_f
  offset = (255 * min) / (min - max).to_f
  h = matrix.length
  w = matrix[0].length
  h.times do |j|
    w.times do |i|
      matrix[j][i] = (matrix[j][i] * slope + offset).to_i
    end
  end
end

def median_filter(matrix)
  filter(matrix) { |neigh| neigh.median }
end

# New filters can be created by defining new Array methods.
class Array
  def median
    sort[length/2]
  end

  def apply_mask(mask)
    zip(mask).inject(0) { |s, x| s + x[0]*x[1] }
  end

  def hsobel
    apply_mask([1, 2, 1, 0, 0, 0, -1, -2, -1])
  end

  def vsobel
    apply_mask([1, 0, -1, 2, 0, -2, 1, 0, -1])
  end

  def sobel
    dx = hsobel
    dy = vsobel
    Math.sqrt(dx*dx + dy*dy).round
  end
end
