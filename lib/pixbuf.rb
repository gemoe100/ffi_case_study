require 'ffi'
require File.expand_path(File.join(File.dirname(__FILE__), "gtk_interface.rb"))

class Pixbuf
  def initialize(arg)
    if arg.kind_of? String
      error = FFI::MemoryPointer.new :pointer
      error.write_pointer(FFI::Pointer::NULL)
      @pixbuf = Gdk::PixbufMethod.new_from_file(arg, error)
      load_ok = error.get_pointer(0).null?
      raise LoadError.new("Couldn't load file #{arg}") if not load_ok
    elsif arg.kind_of? FFI::Pointer
      @pixbuf = arg
    else
      raise TypeError.new("arg should be String or FFI::Pointer to pixbuf")
    end
  end

  def unwrap; @pixbuf; end

  def method_missing(id, *args)
    begin
      Gdk::PixbufMethod.send('get_' << id.to_s, @pixbuf, *args)
    rescue
      raise NoMethodError.new("undefined method `#{id}' for #{self}")
    end
  end

  def save(filename)
    error = FFI::MemoryPointer.new :pointer
    error.write_pointer(FFI::Pointer::NULL)
    format = filename.split('.').last
    Gdk::PixbufMethod.save(@pixbuf, filename, format, error, :pointer, nil)
    save_ok = error.get_pointer(0).null?
    raise Exception.new("couldn't save file #{filename}") if not save_ok
  end

  def size
    (height-1)*rowstride + width*n_channels
  end

  def each_nchannel_slice(&blk)
    if blk
      height.times do |j|
        width.times do |i|
          index = (j*rowstride + i*n_channels)
          indices = (index..index+n_channels-1).to_a
          blk.call(indices)
        end
      end
    else
      to_enum(:each_nchannel_slice)
    end
  end

  def each_pixel
    if block_given?
      each_nchannel_slice do |indices|
        yield indices.map { |index| pixels.get_uchar(index) }
      end
    else
      to_enum(:each_pixel)
    end
  end

  def each_pixel_with_nchannel_slice
    if block_given?
      each_nchannel_slice do |indices|
        pixel = indices.map { |i| pixels.get_uchar(i)}.to_a
        yield [pixel, indices]
      end
    else
      to_enum(:each_pixel_with_nchannel_slice)
    end
  end

  def each_red_index
    each_nchannel_slice.map { |indices| indices[0] }
  end

  def each_red_pixel
    each_red_index.map { |i| pixels.get_uchar(i) }.each
  end

  def each_green_index
    each_nchannel_slice.map { |indices| indices[1] }
  end

  def each_green_pixel
    each_green_index.map { |i| pixels.get_uchar(i) }.each
  end

  def each_blue_index
    each_nchannel_slice.map { |indices| indices[2] }
  end

  def each_blue_pixel
    each_blue_index.map { |i| pixels.get_uchar(i) }.each
  end

  def <<(source)
    Gdk::PixbufMethod.copy_area(
      source.unwrap, 0, 0, source.width, source.height,
      @pixbuf, 0, 0)
  end
end
