require File.expand_path(File.join(File.dirname(__FILE__), "gtk_interface.rb"))
require File.expand_path(File.join(File.dirname(__FILE__), "pixbuf.rb"))

# converting argv from ruby to c
argv = FFI::MemoryPointer.new(:pointer, ARGV.length)
ARGV.each_with_index do |arg, i|
  argv[i].write_pointer(FFI::MemoryPointer.from_string(arg))
end

argc_ptr = FFI::MemoryPointer.new(:int)
argc_ptr.write_uint32(ARGV.length)
argv_ptr = FFI::MemoryPointer.new(:pointer)
argv_ptr.write_pointer(argv)
Gtk.init(argc_ptr, argv_ptr)

window = Gtk::Window.new(:GTK_WINDOW_TOPLEVEL)
box = Gtk::Box::new(:GTK_ORIENTATION_HORIZONTAL, 80)
button = Gtk::Button.new_with_label("Don't be shy, say hi!")
label = Gtk::Label.new('...')
pixbuf = Pixbuf.new(File.join('lib', 'palmae.png'))
image = Gtk::Image.new_from_pixbuf(pixbuf.unwrap)

G::Signal.connect(window, "destroy", Gtk::CALLBACK_main_quit, nil)
G::Signal.connect_swapped(button, "clicked",
	                  proc { |label| Gtk::Label.set_text(label, 'Hi!') },
                          label)

Gtk::Container.add(window, box)
Gtk::Box.pack_start(box, label, true, true, 0)
Gtk::Box.pack_start(box, image, true, true, 0)
Gtk::Box.pack_start(box, button, true, true, 0)
Gtk::Widget.show_all(window)
Gtk.main
