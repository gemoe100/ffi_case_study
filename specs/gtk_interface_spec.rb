require 'rspec'
require 'gtk_interface'

describe "init_or_get_module" do
  it "returns a module if it already exists" do
    module Foo
    end
    init_or_get_module(:Foo).should equal(Foo)
  end
  it "creates a new module if it does not yet exist" do
    mod_bar = init_or_get_module(:Bar)
    mod_bar.class.should equal(Module)
    Bar.should equal(mod_bar)
  end
  it "can also be found in a submodule" do
    module Outer
      module Inner
      end
    end
    init_or_get_module(:Inner, Outer)
  end
  it "can also be created in a submodule" do
    module Outer
    end
    inner_mod = init_or_get_module(:Inner, Outer)
    Outer::Inner.should equal(inner_mod)
  end
end

describe "interface" do
  it "creates an interface to the specified method" do
    # setting up Gtk is not necessary to make test succeed
    # but it gets rid of all the Gtk warnings that are printed on the console
    module SetupGtk
      extend FFI::Library
      ffi_lib 'gtk-3'
      attach_function(:init, :gtk_init, [:pointer, :pointer], :void)
    end
    SetupGtk.init(nil, nil)
    # end setup
    # the actual test starts here
    interface("gtk-3", :gtk, :label, :new,
              [:string], :pointer)
    interface(nil, :gtk, :label, :get_text,
              [:pointer], :string)
    label = Gtk::Label.new("TEST")
    Gtk::Label.get_text(label).should == "TEST"
  end
  it "converts class names to camel case" do
    interface("gtk-3", :gtk, :file_chooser_dialog, :new,
              [:string, :pointer, :int, :varargs], :pointer)
    lambda { Gtk::FileChooserDialog }.should_not raise_error
  end
end

describe Gdk::PixbufMethod do
  G.type_init()
  error = FFI::MemoryPointer.new :pointer
  error.put_pointer(0, FFI::Pointer::NULL)
  pixbuf =
  Gdk::PixbufMethod.new_from_file(File.join('specs', 'palmae.png'), error)
  raise Exception.new("couldn't load file!") if !error.get_pointer(0).null?

  describe '#get_width' do
    it "returns the pixbuf's width" do
      Gdk::PixbufMethod.get_width(pixbuf).should == 30
    end
  end
  describe '#get_height' do
    it "returns the pixbuf's height" do
      Gdk::PixbufMethod.get_height(pixbuf).should == 60
    end
  end
  describe '#get_n_channels' do
    it "returns the pixbuf's n_channels, either 3 (rgb) or 4 (rgba)" do
      Gdk::PixbufMethod.get_n_channels(pixbuf).should == 3
    end
  end
  describe '#get_rowstride' do
    it "returns the pixbuf's rowstride, approximately n_channels*width" do
      Gdk::PixbufMethod.get_rowstride(pixbuf).should == 92
    end
  end
  describe '#save' do
    it "saves the pixbuf to a file" do
      Gdk::PixbufMethod.save(pixbuf, 'copy.png', 'png', error, :pointer, nil)
      File::exists?('copy.png').should be_true
      copy = Gdk::PixbufMethod.new_from_file('copy.png', error)
      Gdk::PixbufMethod.get_height(pixbuf).should ==
      Gdk::PixbufMethod.get_height(copy)
      Gdk::PixbufMethod.get_width(pixbuf).should ==
      Gdk::PixbufMethod.get_width(copy)
      Gdk::PixbufMethod.get_n_channels(pixbuf).should ==
      Gdk::PixbufMethod.get_n_channels(copy)
      Gdk::PixbufMethod.get_rowstride(pixbuf).should ==
      Gdk::PixbufMethod.get_rowstride(copy)
      orig_pixels = Gdk::PixbufMethod.get_pixels(pixbuf)
      copy_pixels = Gdk::PixbufMethod.get_pixels(copy)
      size = Gdk::PixbufMethod.get_height(pixbuf)-1 *
             Gdk::PixbufMethod.get_rowstride(pixbuf) +
             Gdk::PixbufMethod.get_width(pixbuf)-1 *
             Gdk::PixbufMethod.get_n_channels(pixbuf)
      0.upto(size) do |b|
        orig_pixels.get_uchar(b).should == copy_pixels.get_uchar(b)
      end
    end
  end

  after(:each) do
    File::delete 'copy.png' if File::exists? 'copy.png'
  end
end
