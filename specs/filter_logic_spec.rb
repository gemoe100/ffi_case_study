require 'rspec'
require 'filter_logic'

describe 'alloc_matrix' do
  it "creates an array containing height arrays containing width elements" do
    matrix = alloc_matrix(width = 5, height = 8)
    matrix.length.should == 8
    matrix.each { |row| row.length.should == 5 }
  end
  it "doesn't use the same memory for all the rows" do
    matrix = alloc_matrix(2, 2)
    matrix[0].should_not equal(matrix[1])
  end
  it "'s elements are 0 if nothing else is mentioned" do
    matrix = alloc_matrix(3, 2)
    matrix.each { |row| row.each { |elem| elem.should == 0 } }
  end
  it "'s elements are elm if given" do
    matrix = alloc_matrix(3, 1, elm = ' ')
    matrix.each { |row| row.each { |elem| elem.should == ' ' } }
  end
end

describe 'neighbors' do
  matrix = ["abcd",
            "efgh",
            "ijkl",
            "mnop"]
  it "returns the values of the given index and it's neighbors." do
    actual = neighbors(matrix, [1, 2])
    expected = ['e', 'f', 'g', 'i', 'j', 'k', 'm', 'n', 'o']
    actual.should == expected
  end
  it "can be used on border indices." do
    neighbors(matrix, [0, 0]).join.should == "aabaabeef"
    neighbors(matrix, [3, 1]).join.should == "cddghhkll"
    neighbors(matrix, [2, 3]).join.should == "jklnopnop"
  end
end

describe Array do
  describe '#median' do
    it "calculates the median" do
      [5, 10, 3, 7, 11].median.should == 7
      "becdfag".chars.to_a.median.should == 'd'
    end
    it "takes the greater index on even array lengths" do
      [0, 1, 2, 3].median.should == 2
      [4, 5].median.should == 5
    end
    it "returns the element of an array with length 1" do
      [:elem].median.should == :elem
    end
    it "returns nil on empty array" do
      [].median.should equal(nil)
    end
  end

  describe '#apply_mask' do
    it "sums up the products of the array and the mask values" do
      [1, 2, 3].apply_mask([0, 0, 0]).should == 0
      [11, 12, 13].apply_mask([1, 1, 1]).should == 11 + 12 + 13
    end
  end

  describe '#hsobel' do
    it "is an implementation of the horizontal sobel operator" do
      x = 9.times.map { Random.rand(255) }.to_a
      expected = x[0] + 2*x[1] + x[2] - x[6] - 2*x[7] - x[8]
      actual = x.hsobel
      actual.should == expected
    end
    it "returns at least -(4*max) and +(4*max) at most
        assuming that the values of the array are between (0..max)" do
      max = Random.rand(1000)
      [max, max, max, 1, 2, 3, 0, 0, 0].hsobel.should == +(4*max)
      [0, 0, 0, 1, 2, 3, max, max, max].hsobel.should == -(4*max)
    end
  end

  describe '#vsobel' do
    it "is an implementation of the vertical sobel operator" do
      x = 9.times.map { Random.rand(255) }.to_a
      expected = x[0] + 2*x[3] + x[6] - x[2] - 2*x[5] - x[8]
      actual = x.vsobel
      actual.should == expected
    end
    it "returns at least -(4*max) and +(4*max) at most
        assuming that the values of the array are between (0..max)" do
      max = Random.rand(1000)
      [max, 1, 0, max, 2, 0, max, 3, 0].vsobel.should == +(4*max)
      [0, 1, max, 0, 2, max, 0, 3, max].vsobel.should == -(4*max)
    end
  end

  describe '#sobel' do
    it "returns the rounded length of the (hsobel, vsobel) vector" do
      [0, 0.5, 2, 3, 0, 0, 0, 0, 0].sobel.should == 5
      [1, 0, 0, 0, 0, 0, 0, 0, 0].sobel.should == 1
      [0, 1, 0, 1, 0, 0, 0, 0, 0].sobel.should == 3
    end
  end
end

describe 'filter' do
  it "filters the given matrix using the given block" do
    matrix = [[1, 1, 2, 1],
              [1, 2, 2, 1],
              [1, 2, 3, 2],
              [1, 1, 2, 2]]
    actual = filter(matrix) { |neigh| neigh.max }
    expected = [[2, 2, 2, 2],
                [2, 3, 3, 3],
                [2, 3, 3, 3],
                [2, 3, 3, 3]]
    actual.should == expected
  end
end

describe 'uchar_scale!' do
  it "scales the matrix such that every entry is a uchar" do
    matrix = [[-128, 127],
              [   0,  64]]
    uchar_scale!(matrix)
    matrix.should == [[  0, 255],
                      [128, 192]]
    # another example, just to make sure:
    matrix = [[200, 220, 240],
               [260, 280, 300]]
    uchar_scale!(matrix)
    matrix.should == [[  0,  51, 102],
                       [153, 204, 255]]
  end
  it "only scales if neccessary" do
    matrix = [[  5,  10,  20],
              [100, 200, 190]]
    uchar_scale!(matrix)
    matrix.should == [[  5,  10,  20],
                      [100, 200, 190]]
  end
end

describe 'median_filter' do
  it "runs the median filter over the given matrix" do
    matrix = ["abc",
              "def",
              "ghi"]
    actual = median_filter(matrix).map { |row| row.join }
    expected = ["bcc",
                "def",
                "ggh"]
    actual.should == expected
  end
end
