require 'rspec'
require 'pixbuf'
require 'gtk_interface'

describe Pixbuf do
  G.type_init()

  # The testing pixbuf has 2 rows and 3 columns and no alpha values and looks like so:
  # ------------------------
  # | black | white | red  |
  # | green | blue  | gray |
  # ------------------------
  # (gray means [128, 128, 128])
  c_pixbuf = Gdk::PixbufMethod.new(:GDK_COLORSPACE_RGB, false, 8, 3, 2)
  rowstride = Gdk::PixbufMethod.get_rowstride(c_pixbuf)
  pixels = Gdk::PixbufMethod.get_pixels(c_pixbuf)
  # black
  pixels.put_uchar(0, 0);
  pixels.put_uchar(1, 0);
  pixels.put_uchar(2, 0);
  # white
  pixels.put_uchar(3, 255);
  pixels.put_uchar(4, 255);
  pixels.put_uchar(5, 255);
  # red
  pixels.put_uchar(6, 255);
  pixels.put_uchar(7, 0);
  pixels.put_uchar(8, 0);
  # green
  pixels.put_uchar(rowstride+0, 0);
  pixels.put_uchar(rowstride+1, 255);
  pixels.put_uchar(rowstride+2, 0);
  # blue
  pixels.put_uchar(rowstride+3, 0);
  pixels.put_uchar(rowstride+4, 0);
  pixels.put_uchar(rowstride+5, 255);
  # gray
  pixels.put_uchar(rowstride+6, 128);
  pixels.put_uchar(rowstride+7, 128);
  pixels.put_uchar(rowstride+8, 128);
  pixbuf = Pixbuf.new(c_pixbuf)

  describe '#method_missing' do
    it "delegates to Gdk::PixbufMethod, adding a get_ prefix" do
      pixbuf.width.should == Gdk::PixbufMethod.get_width(c_pixbuf)
    end
    it "raises the familiar error if delegation fails" do
      lambda { pixbuf.unknown }.should
        raise_error(NoMethodError, "undefined method unknown for #{pixbuf}")
    end
  end

  describe '#unwrap' do
    it "it's nothing more than an attr_reader.
        It's just that 'unwrap' seems more natural in the Wrapper context" do
      class Pixbuf; attr_reader :pixbuf; end
      pixbuf.unwrap.should equal(pixbuf.pixbuf)
      class Pixbuf; undef_method :pixbuf; end
    end
  end

  describe '#initialize' do
    it "behaves like a wrapper when given an FFI::Pointer to a pixbuf" do
      Pixbuf.new(pixbuf.unwrap).unwrap.should == pixbuf.unwrap
    end
    it "raises TypeError if neither a string nor an FFI::Pointer to a pixbuf was given" do
      lambda {Pixbuf.new(:bad_input)}.should raise_error(TypeError)
    end
  end

  describe '#size' do
    it "returns the buffer's total size" do
      pixbuf.size.should ==
        (pixbuf.height-1)*pixbuf.rowstride + pixbuf.width*pixbuf.n_channels
    end
  end

  describe '#each_nchannel_slice' do
    it "passes n_channel sized slices to the given block until size" do
      rowstride = pixbuf.rowstride
      expectations = [[0, 1, 2],
                      [3, 4, 5],
                      [6, 7, 8],
                      [rowstride+0, rowstride+1, rowstride+2],
                      [rowstride+3, rowstride+4, rowstride+5],
                      [rowstride+6, rowstride+7, rowstride+8]]
      pixbuf.each_nchannel_slice.zip(expectations) do |actual, expected|
        actual.should == expected
      end
    end
  end

  describe '#each_pixel' do
    it "passes each pixel to the given block" do
      expectations = [[  0,   0,   0], [255, 255, 255], [255,   0,   0],
                      [  0, 255,   0], [  0,   0, 255], [128, 128, 128]]
      pixbuf.each_pixel.zip(expectations) do |actual, expected|
        actual.should == expected
      end
    end
  end

  describe '#each_pixel_with_nchannel_slice' do
    it "passes each pixel and it's indices to the given block" do
      pixbuf.each_pixel_with_nchannel_slice do |pixel, indices|
        pixel.zip(indices) do |value, index|
          value.should == pixbuf.pixels.get_uchar(index)
        end
      end
    end
  end

  describe '#each_red_index' do
    it "only passes the indices of the red pixel components to the given block" do
      expectations = [0, 3, 6, rowstride+0, rowstride+3, rowstride+6]
      pixbuf.each_red_index.zip(expectations) do |actual, expected|
        actual.should == expected
      end
    end
  end

  describe '#each_red_pixel' do
    it "only passes the red pixel components to the given block" do
      expectations = [ 0, 255, 255, 0, 0, 128]
      pixbuf.each_red_pixel.zip(expectations) do |actual, expected|
        actual.should == expected
      end
    end
  end

  describe '#each_green_index' do
    it "only passes the indices of the green pixel components to the given block" do
      expectations = [1, 4, 7, rowstride+1, rowstride+4, rowstride+7]
      pixbuf.each_green_index.zip(expectations) do |actual, expected|
        actual.should == expected
      end
    end
  end

  describe '#each_green_pixel' do
    it "only passes the green pixel components to the given block" do
      expectations = [ 0, 255, 0, 255, 0, 128]
      pixbuf.each_green_pixel.zip(expectations) do |actual, expected|
        actual.should == expected
      end
    end
  end

  describe '#each_blue_index' do
    it "only passes the indices of the blue pixel components to the given block" do
      expectations = [2, 5, 8, rowstride+2, rowstride+5, rowstride+8]
      pixbuf.each_blue_index.zip(expectations) do |actual, expected|
        actual.should == expected
      end
    end
  end

  describe '#each_blue_pixel' do
    it "only passes the blue pixel components to the given block" do
      expectations = [ 0, 255, 0, 0, 255, 128]
      pixbuf.each_blue_pixel.zip(expectations) do |actual, expected|
        actual.should == expected
      end
    end
  end
end
